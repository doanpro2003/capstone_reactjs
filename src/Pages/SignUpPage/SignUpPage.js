import React from "react";
import Lottie from "lottie-react";
import bg_animate from "../../assets/animate.json";
import { ErrorMessage, Field, Form, Formik } from "formik";
import { backgroundImageStyle } from "../../Css/SignUpCss/SignUpCss";
import { initialSignUp, signUpUserSchema } from "./utilsSignUp";
import { postSignUp } from "../../service/userService";
import { message } from "antd";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { setUserInfo } from "../../redux-toolkit/userSlice";
import { userLocalService } from "../../service/localService";

export default function SignUpPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const handleSignUp = (values) => {
    postSignUp(values)
      .then((res) => {
        message.success("Đăng ký thành công!");
        dispatch(setUserInfo(res.data.content));
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đăng ký thất bại");
      });
  };
  return (
    <div
      className="h-screen w-screen flex justify-center items-center"
      style={backgroundImageStyle}
    >
      <div className="grid grid-cols-1 sm:grid-cols-2 container p-5 rounded backdrop-blur-sm bg-white/10">
        <div className="hidden sm:block">
          <Lottie
            className="w-full h-full object-cover"
            animationData={bg_animate}
            loop={true}
          />
        </div>
        <div className="p-10  rounded-md bg-gradient-to-tl via-red-400 to-yellow-500">
          <h2 className="text-center text-4xl dark:text-white font-bold">
            Đăng ký
          </h2>
          <Formik
            initialValues={initialSignUp}
            validationSchema={signUpUserSchema}
            onSubmit={handleSignUp}
            render={(formikProps) => (
              <Form>
                <div className="flex flex-col py-2">
                  <label className="text-white">Tài khoản: </label>
                  <Field
                    type="text"
                    className="rounded-lg bg-white p-1 focus:bg-gray-300 focus:outline-none"
                    name="taiKhoan"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="taiKhoan">
                    {(msg) => <div className="  text-red-600">{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="flex flex-col py-2">
                  <label className="text-white">Mật khẩu: </label>
                  <Field
                    type="password"
                    className="rounded-lg bg-white p-1 focus:bg-gray-300 focus:outline-none"
                    name="matKhau"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="matKhau">
                    {(msg) => <div className="  text-red-600">{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="flex flex-col py-2">
                  <label className="text-white">Họ tên: </label>
                  <Field
                    type="text"
                    className="rounded-lg bg-white p-1 focus:bg-gray-300 focus:outline-none"
                    name="hoTen"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="hoTen">
                    {(msg) => <div className="  text-red-600">{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="flex flex-col py-2">
                  <label className="text-white">Email: </label>
                  <Field
                    type="email"
                    className="rounded-lg bg-white p-1 focus:bg-gray-300 focus:outline-none"
                    name="email"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="email">
                    {(msg) => <div className="  text-red-600">{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="flex flex-col py-2">
                  <label className="text-white">Số điện thoại: </label>
                  <Field
                    type="text"
                    className="rounded-lg bg-white p-1 focus:bg-gray-300 focus:outline-none"
                    name="soDT"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="soDT">
                    {(msg) => <div className="  text-red-600">{msg}</div>}
                  </ErrorMessage>
                </div>
                <button className="w-full text-white font-bold rounded-lg my-1 py-2 bg-blue-500 shadow-lg shadow-blue-500/50 hover:shadow-blue-500/40">
                  Đăng ký
                </button>
              </Form>
            )}
            
          />
        </div>
      </div >
    </div >
  );
}
