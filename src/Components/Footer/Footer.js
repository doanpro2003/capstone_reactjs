/* eslint-disable jsx-a11y/anchor-is-valid */
import { AppleOutlined } from "@ant-design/icons";
import { AndroidOutlined } from "@ant-design/icons/lib/icons";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../service/movieService";
import { createFromIconfontCN } from "@ant-design/icons";
import { Space } from "antd";

export default function Footer() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    getMovieByTheater()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => { });
  }, []);
  const arrHeThongRap = _.map(dataMovie, (heThongRap) =>
    _.pick(heThongRap, ["maHeThongRap", "tenHeThongRap", "logo"])
  );
  const IconFont = createFromIconfontCN({
    scriptUrl: "//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js",
  });
  return (
    <footer className="py-6 dark:bg-gray-900 dark:text-gray-50">
      <div className="container px-6 mx-auto space-y-6 divide-y divide-gray-400 md:space-y-12 divide-opacity-50">
        <div className="grid grid-cols-12">
          <div className="pb-6 col-span-full md:pb-0 md:col-span-3">
          </div>
          <div className="col-span-6 text-center md:text-left md:col-span-3 hidden sm:block">
            <p className="pb-1 font-medium">ĐỐI TÁC</p>
            <div className="grid grid-cols-3">
              {arrHeThongRap.map((item, index) => {
                return (
                  <div key={index}>
                    <img src={item.logo} alt="logo" style={{ width: 50 }} />
                  </div>
                );
              })}
            </div>
          </div>
          <div className="col-span-6 text-center md:text-left md:col-span-3">
            <p className="pb-1 font-medium">MOBILE APP</p>
            <ul>
              <AppleOutlined className="text-3xl" />
              <AndroidOutlined className="pl-3 text-3xl" />
            </ul>
          </div>
          <div className="col-span-6 text-center md:text-left md:col-span-3">
            <p className="pb-1 font-medium">SOCIAL</p>
            <ul>
              <Space className="text-2xl">
                <IconFont type="icon-tuichu" />
                <IconFont type="icon-facebook" />
                <IconFont type="icon-twitter" />
              </Space>
            </ul>
          </div>
        </div>
        <div className="grid justify-center items-center pt-6 lg:justify-betweens">
          <div className="flex flex-col self-center text-sm text-center md:block lg:col-start-1 md:space-x-6">
            <span>©2023 All rights reserved</span>
            <a rel="noopener noreferrer" href="#">
              <span>Privacy policy</span>
            </a>
            <a rel="noopener noreferrer" href="#">
              <span>Terms of service</span>
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}
