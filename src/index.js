import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { configureStore } from "@reduxjs/toolkit";
import filmDetailSlice from "./redux-toolkit/filmDetailSlice";
import { Provider } from "react-redux";
import userSlice from "./redux-toolkit/userSlice";
import spinnerSlice from "./redux-toolkit/spinnerSlice";

const root = ReactDOM.createRoot(document.getElementById("root"));

export const store_toolkit = configureStore({
  reducer: {
    filmDetailSlice: filmDetailSlice,
    userSlice: userSlice,
    spinnerSlice: spinnerSlice,
  },
});

root.render(
  <Provider store={store_toolkit}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
