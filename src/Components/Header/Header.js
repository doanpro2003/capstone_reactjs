import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="py-5 px-20 flex justify-between items-center shadow-lg">
      <NavLink to={"/"}>
        <span className="text-red-600 text-3xl font-medium">
          CyberFlix
        </span>
      </NavLink>
      <UserNav />
    </div>
  );
}
