import backgroundImage from "../../assets/anh.jpeg";

// background sign-up
export const backgroundImageStyle = {
  backgroundImage: `url("${backgroundImage}")`,
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
};
