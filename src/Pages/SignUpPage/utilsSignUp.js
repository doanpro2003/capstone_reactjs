import * as yup from "yup";

export const initialSignUp = {
  taiKhoan: "",
  matKhau: "",
  hoTen: "",
  email: "",
  soDT: "",
  maNhom: "GP01",
};

// validation
export const signUpUserSchema = yup.object().shape({
  taiKhoan: yup.string().required("* Vui lòng nhập tài khoản!"),
  matKhau: yup.string().required("* Vui lòng nhập mật khẩu!"),
  hoTen: yup.string().required("* Vui lòng nhập họ tên!"),
  email: yup
    .string()
    .required("* Vui lòng nhập email!")
    .email("* Email không hợp lệ!"),
  soDT: yup
    .string()
    .required("* Vui lòng nhập số điện thoại!")
    .matches(/^[0-9]+$/),
  maNhom: yup.string().required("* Field is required!"),
});
