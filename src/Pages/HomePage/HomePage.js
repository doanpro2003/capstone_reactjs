import React, { useEffect, useState } from "react";
import { getMovieList } from "../../service/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTab/MovieTab";
export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    getMovieList()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => { });
  }, []);
  return (
    <div>
      <div className="container px-10 pt-5">
        <MovieList movieArr={movieArr} />
        <MovieTab />
      </div>
    </div>
  );
}
