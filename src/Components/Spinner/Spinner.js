import React from "react";
import { useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";

export default function Spinner() {
  let isLoading = useSelector((state) => {
    return state.spinnerSlice.isLoading;
  });
  return isLoading ? (
    <div className="fixed bg-black w-screen h-screen top-0 l-0 z-50 flex justify-center items-center">
      <BeatLoader size="50" speedMultiplier={1} color="#EB455F" />
    </div>
  ) : (
    <></>
  );
}
